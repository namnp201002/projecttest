#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin >> t;
    while(t--){
        long long x, y;
        cin >> x >> y;
        long long a, b;
        cin >> a >> b;
        long long sum = (x+y)*a;
        if(x<=y){
            long long sum2= a*(y-x) + x*b;
            if(sum2<=sum){
                cout << sum2;
            }else cout<<sum;
        }else{
            long long sum2 = a*(x-y) + y*b;
            if(sum2<=sum ) cout<<sum2;
            else cout<<sum;
        }
        cout<<endl;
    }
    system("pause");
    return 0;
}