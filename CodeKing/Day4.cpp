#include<bits/stdc++.h>
using namespace std;
int snt (int n) {
	if (n<2)	return 0;
	for (int i=2; i<=sqrt(n); i++) {
		if (n%i == 0)	return 0;
	}
	return 1;
}

int main () {
	int t;
	cin >> t;
	while (t--) {
		int n, sum = 0, max = 0;
		char a[10000]={};
		cin >> n;
		cin.ignore();
		for (int i=0; i<n; i++) {
			cin >> a[i];
			sum += a[i]-'0';
			if (max < a[i]-'0')	max = a[i]-'0';
		}
		if (snt(sum) == 1)	cout << "NO" << endl;
		else if (max*2 > sum)	cout << "NO" << endl;
		else {
			int s = 0, sl[10000], check = -1;
			for (int i=max; i<=sum/2; i++) {
				check = -1;
				if (sum % i == 0) {
					s = 0;
					int k = sum/i;
					int k0 = 0;
					for (int m=0; m<n; m++) {
						s += a[m]-'0';
						if (s == i) {
							s = 0;
							k0++;
						}
					}
					if (k0 == k) {
						check = 1;
						break;
					}
				}
			}
			if (check != 1)	cout << "NO" << endl;
			else	cout << "YES" << endl;
		}
	}
	return 0;
}