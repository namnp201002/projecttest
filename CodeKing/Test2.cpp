#include<bits/stdc++.h>
using namespace std;

long long power(int x, unsigned int y)
{
    int temp;
    if( y == 0)
        return 1;
    temp = power(x, y / 2);
    if (y % 2 == 0)
        return temp * temp;
    else
        return x * temp * temp;
}
int main(){
    int t;
    cin >> t;
    while(t--){
        long long n, tich=1;
        cin >> n;
        if(n==0) cout<<"0"<<endl;
        else if(n==1) cout<<"1"<<endl;
        else{
            if(n>=3){
                int k=n/3;
                n=n- k*3;
                tich *= power(3,k);
            }
            if(n>=2){
                int k=n/2;
                n=n-k*2;
                tich *= power(2,k);
            }
            cout<<tich%1000000007<<endl;
        }
    }
    system("pause");
    return 0;
}
