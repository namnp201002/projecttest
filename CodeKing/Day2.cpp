#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin >> t;
    while(t--){
        long long a, b, N;
        cin >> a>> b>> N;
        if(N<(abs(a)+abs(b))){
            cout<<"NO"<<endl;
        }else{
            long long x = N-(abs(a)+abs(b));
            if(x%2==0){
                cout << "YES"<<endl;
            }else cout<<"NO"<<endl;
        }
    }
    // system("pause");
    return 0;
}