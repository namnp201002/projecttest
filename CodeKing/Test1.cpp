#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin >> t;
    while(t--){
        int n;
        cin >> n;
        long long a[n+1];
        int dem=0;
        int du1=0, du2=0;
        for(int i=0; i<n; i++){
            cin >>a[i];
            if(a[i]%3==0 && a[i]!=0){
                dem++;
            }
            if(a[i]%3==1) du1++;
            if(a[i]%3==2) du2++;
        }
        int x=0;
        if(du1<=du2){
            x=du1;
            int y=(int)(du2-x)/3 ;
            cout<<dem+x+y;
        }else{
            x=du2;
            int y=(int)(du1-x)/3;
            cout<<dem+x+y;
        }
        cout<<endl;
    }
    system("pause");
    return 0;
}