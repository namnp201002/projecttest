#include<stdio.h>
#include<stdlib.h>
typedef struct PhanSo{
    long long Tu;
    long long Mau;
}PS;
long long UCLN(long long a, long long b){
    if (a==0 || b==0){
        return a+b;
    }
    while (a!=b){
        if (a>b){
            a-=b;
        }else{
            b-=a;
        }
    }
    return a;
}
void RutGon(long long a, long long b){
    if(a%b==0){
        printf("%lld\n", a/b);
    }else{
        long long c=UCLN(a,b);
        printf("%lld/%lld\n", a/c, b/c);   
    }
}
void QuyDong(PS ps1, PS ps2){
    long long m1=ps1.Mau, m2=ps2.Mau;
    long long x1=ps1.Tu, y1=ps1.Mau, x2=ps2.Tu, y2=ps2.Mau;
    if(x1%y1==0 && x2%y2==0){
        long long x=ps1.Tu/ps1.Mau, y=ps2.Tu/ps2.Mau;
        printf("%lld %lld", x, y);
        printf("\n");
        printf("%lld\n", x+y);
        RutGon(x,y);
    }else if(x1%y1==0 && x2%y2!=0){
        ps1.Tu= ps1.Tu/ps1.Mau;
        ps1.Mau = 1;
        long long a=ps1.Tu*ps2.Mau;
        long long b=ps1.Mau*ps2.Mau;
        printf("%lld/%lld %lld/%lld\n", a, b, ps2.Tu, ps2.Mau);
        long long x1=a+ps2.Tu;
        long long y1=ps2.Mau;
        RutGon(x1,y1);
        long long x2=a*ps2.Mau, y2=b*ps2.Tu;
        RutGon(x2,y2);
    }else if(x1%y1!=0 && x2%y2==0){
        ps2.Tu= ps2.Tu/ps2.Mau;
        ps2.Mau = 1;
        long long a=ps2.Tu*ps1.Mau, b=ps2.Mau*ps1.Mau;
        printf("%lld/%lld %lld/%lld\n", ps1.Tu, ps1.Mau, a, b);
        long long x1=ps1.Tu+a, y1=ps1.Mau;
        RutGon(x1,y1);
        long long x2=ps1.Tu*b, y2=ps1.Mau*a;
        RutGon(x2,y2);
    }else {
        long long c = m1*m2/UCLN(m1,m2);
        long long a = ps1.Tu*(c/ps1.Mau), b=ps2.Tu*(c/ps2.Mau);
        printf("%lld/%lld %lld/%lld\n", a, c, b, c);
        long long x1=a+b, y1=c;
        RutGon(x1,y1);
        RutGon(a,b);
    }
}
int main(){
    int t;
    scanf("%d", &t);
    for(int i=1; i<=t; i++){
        PS ps1, ps2;
        scanf("%lld", &ps1.Tu);
        scanf("%lld", &ps1.Mau);
        scanf("%lld", &ps2.Tu);
        scanf("%lld", &ps2.Mau);
        printf("Case #%d:\n", i);
        QuyDong(ps1, ps2);
    }
    // system("pause");
    return 0;
}
